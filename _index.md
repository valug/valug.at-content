---
date: '2021-06-13 11:23:25'
moinmoin_path: /Index/revisions/00000039
title: Index
---

# Was ist die VALUG?
Die VALUG ist ein Treffpunkt für Fans von freien Betriebssystemen und freier Software. Allen voran natürlich GNU/Linux-Fans, aber auch Anhänger/innen diverser BSD-Varianten finden sich immer wieder bei uns ein.

VALUG steht für "Voralpen Linux User Group" und bei uns ist jede:r willkommen, wir wollen möglichst viele Menschen erreichen.

Es gibt keine Einstiegshürden und es wird kein Vorwissen vorausgesetzt. Natürlich sind auch Profis willkommen und in bester Gesellschaft. Die Themenbereiche, die wir bei unseren Treffen, in Mailinglisten oder im Chat behandeln, sind dementsprechend breit gefächert (siehe vergangene Veranstaltungen)

Wir treffen uns zu Stammtischen (immer der 2. Freitag im Monat) im [VALUG-Klubraum](/pages/Klubraum), im [Chat](/pages/Chat) oder in der [Mailingliste](/pages/Mailingliste).

Diese Website wird von [hugo](https://gohugo.io) gerendert, der [Inhalt](https://gitlab.com/valug/valug.at-content) sowie die [Konfiguration](https://gitlab.com/valug/valug.at) liegen auf Gitlab. Bei Änderungsvorschlägen bitte einfach ein Issue oder einen Merge Request erstellen.

# Die nächsten Termine
[Kalender abonnieren](/events/index.ics)

[Flyer und Plakate für die kommenden Veranstaltungen zum Ausdrucken](/pages/Öffentlichkeitsarbeit)

## Ältere Projekte

[Linux Presentation Day 2015](/pages/LinuxPresentationDay2015)

[Kalliope](/pages/Kalliope)
