---
date: '2015-10-28 18:50:56'
moinmoin_path: /LinuxPresentationDay2015/revisions/00000011
title: LinuxPresentationDay2015
---

![attachment:linux-presentation-day-logo.png](/files/linux-presentation-day-logo.png)

# Linux Presentation Day 2015 in Wels
 * Datum: **Sa, 14.11.2015 9:00-17:00**
 * **Eintritt frei, keine Anmeldung erforderlich**
 * Ort: **[Computerclub_Wels,_Stelzhamerstraße_1,_4600_Wels](//Location)**

## Programm
## Ganztägig
 * Alte Computer wiederbeleben mit Linux
 * Computer für Flüchtlinge (Projekt Kalliope)
 * Mediacenter mit Raspberry PI
 * Linux-Computer für Kinder: Spiele, Lernprogramme, Zeichenprogramme
 * Computerspiele unter Linux

## Vorträge
Die Vorträge dauern jewels ca. 45-60 Minuten
 * **Die Veranstalter stellen sich vor**
  * Um 11:00
  * [VALUG](http://valug.at/), [Computerclub_Wels](http://ccwels.net/), [FunkFeuer_Wels](http://wels.funkfeuer.at/) berichten über sich und ihre Ziele
 ***Bericht über das Projekt Kalliope**
  * Um 13:00
  * Die VALUG belebt alte Rechner mit Hilfe von Linux neu für Flüchtlinge
 * **Freie Software-Lizenzen**
  * Um 15:00
  * Was ist an freier Software wie Linux anders als an „herkömmlicher“, und was hat es mit diesen komischen Software-Lizenzen auf sich?

## Über den Linux Presentation Day

Der [Linux_Presentation_Day](http://www.linux-presentation-day.de) ist eine Veranstaltung, auf der Leute, die Linux gar nicht oder kaum kennen, einen Einblick in den Umgang mit dem Open-Source-Betriebssystem bekommen können. Unterschiedliche Linux-Varianten werden in typischen Alltagssituationen vorgeführt und können ausprobiert werden. Das Ziel der Veranstaltung, die kostenlos besucht werden kann, ist es, den Besuchern bei der Beantwortung der Frage zu helfen, ob auch sie irgendwann in Zukunft Linux nutzen wollen, als Ersatz für oder Ergänzung zu Windows.

Wer sich nach dem Besuch des Linux Presentation Day dafür entscheidet, Linux auf seinem eigenen Computer auszuprobieren, kann dafür auf den monatlichen Treffen der VALUG kostenlos Hilfe bekommen.

## Über den Veranstalter

Die VALUG ist ein Treffpunkt für Fans von freien Betriebssystemen und freier Software. Allen voran natürlich GNU/Linux-Fans, aber auch Anhänger/innen diverser BSD-Varianten finden sich immer wieder bei uns ein.

VALUG steht für "Voralpen Linux User Group". Da die VALUG kein Verein und keine Firma ist, braucht man sich nur zugehörig fühlen, dann ist man Mitglied!

Bei uns ist Jede und Jeder willkommen und wir wollen möglichst viele Menschen erreichen. Es gibt keine Einstiegshürden und es wird kein Vorwissen vorausgesetzt. Natürlich sind auch Profis willkommen und in bester Gesellschaft. Die Themenbereiche, die wir bei unseren Treffen, in Mailinglisten oder im Chat behandeln, sind dementsprechend breit gefächert.

Wir treffen uns zu Stammtischen (immer der 2. Freitag im Monat) im [VALUG-Klubraum](/pages/Klubraum), im [IRC](/pages/Chat) Channel (ein Chat) oder in der [Mailingliste](/pages/Mailingliste). Im Wiki können registrierte !NutzerInnen Artikel schreiben.

Weitere Informationen zu unseren Treffen und den sonstigen Aktivitäten und -angeboten finden Sie [hier](/).

## Organisatorisches
Organisatorisches ist [hier](/pages/LinuxPresentationDay2015/Organisation) zu finden.
