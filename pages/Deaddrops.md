---
date: '2012-05-23 14:14:16'
moinmoin_path: /Deaddrops/revisions/00000001
title: Deaddrops
---
# Deaddrops
Die VALUG hat im Großraum Wels mehrere Deaddrops installiert. Deaddrops sind USB-Sticks, die zum anonymen Offline-Austausch von Dateien an öffentlich zugänglichen Plätzen eingemauert werden.

Mehr Informationen über Deaddrops sind auf [http://www.deaddrops.com/](http://www.deaddrops.com/) zu finden.

## Von der VALUG installierte Deaddrops
 * [Alter_Schl8hof,_Laderampe_hinter_dem_Haus](http://deaddrops.com/db/?page=view&id=1017)
 * [Unter_der_Zugbrücke_zwischen_Thalheim_und_Wels](http://deaddrops.com/db/?page=view&id=1018)
