---
date: '2019-07-12 19:08:44'
moinmoin_path: /Getr(c3a4)nke/revisions/00000002
title: "Getr\xE4nke"
---
# Getränke im VALUG-Klubraum

Wir haben im [VALUG-Klubraum](/pages/Klubraum) meistens einige Kisten Getränke:
 * Limonaden
 * Mate

Die leeren Kisten können in verschiedenen Geschäften zurückgegeben werden. Leider nicht alle Kisten in allen Geschäften. Welche Flaschen drin sind, ist egal, die sind bis auf das Label alle gleich. Daher hier eine Compatibility Matrix:


|| ||**Riba**||**…**||
|| Ritter || ✓ || ||
|| Flora Mate || ✓ || ||
|| Club Mate || ✗ || ||
|| Braucommune Freistadt || ✗ || ||
|| Brauerei Ried || ? || ||
