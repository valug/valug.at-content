---
date: '2012-01-29 19:28:11'
moinmoin_path: /vmcluster/revisions/00000011
title: vmcluster
---
## the idea

Testing different clustersetups/strategies with free/open/other software. Different storage backends, different hypervisors, different management software.

Location: @liwoli 2012 as a workshop

## hypervisors

 * kvm (openstack, ganetti, sheepdog, proxmox) 
 * xen (xenserver, openstack, ganetti, ...)
 * vmware (vmware)

## storage backends

 * replicated with drbd/heartbeat&co
   * iscsi
   * nfs4/files
   * local lvm

 * single homed
   * iscsi
   * nfs4/files
   * local lvm

 * ceph + rbd + libvirt 

## Test setup

## link dump

http://www.markround.com/archives/44-Building-a-redundant-iSCSI-and-NFS-cluster-with-Debian-Part-1.html

http://wiki.osuosl.org/public/ganeti/index

http://www.osrg.net/sheepdog/

http://www.openstack.org/

http://devstack.org/

https://github.com/dellcloudedge/crowbar

http://pve.proxmox.com/

http://berrange.com/posts/2011/10/12/setting-up-a-ceph-cluster-and-exporting-a-rbd-volume-to-a-kvm-guest/

## network
 * (HP ProCurve) Switches with VLAN functionality

## server

there is enough hardware available, more at a later time
