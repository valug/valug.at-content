---
date: '2011-09-05 11:40:16'
moinmoin_path: /Bibliothek(2f)B(c3bc)cher/revisions/00000013
title: "B\xFCcher"
---
# Bücher in der VALUG-Bibliothek

... to be continued. Hier ist im Moment nur ein sehr kleiner Teil der Bücher gelistet (Zeitmangel....)

![drawing:klein_Biblio_1.jpg](../klein_Biblio_1.jpg)

![drawing:klein_Biblio_2.jpg](../klein_Biblio_2.jpg)

![drawing:klein_Biblio_3.jpg](../klein_Biblio_3.jpg)

![drawing:klein_Biblio_4.jpg](../klein_Biblio_4.jpg)

|| **Titel**   || **Autor**                || Verlag || **Besitzer** || im moment ausgeborgt (von) ||
|| Openstreetmap || Frederik Ramm, Jochen Topf || -|| [silwol](/people/silwol)     ||-||
|| The Complete Red Hat Linux 5.2 Installation Guide || - || - || - || - ||
|| SuSE Linux Administrationshandbuch || - || - || - || - ||
|| Checkpoint Next Generation || -|| mitp || pv || - ||
|| Digitaltechnik || Erich Leonhardt || - || - || - ||
|| Software Engineeing mit Unix-Workstations || - || - || - || - ||
|| sendmail || --|| o'reilly || pv || - ||
|| qmail Das Handbuch || || mitp || pv || - ||
|| KOMA Script || Kohm Morawski || dante || - || - ||
|| codes || simon singh ||- || - || - ||
|| embedded systeme mit linux || - || franzis || - || - ||
|| linux - wegweiser zur installation & konfiguration ||- || o'reilly || pv || - ||
|| netware für dummies ||- || mitp || pv || - ||
|| linux systemadministration ||- ||- || pv || - ||
|| tcp/ip netzwerk administration || -|| o'reilly || pv || - ||
|| the cathedral and the bazar || eric s raymond || o'reilly || pv || - ||
|| künstliche intelligenz || george f. luger || pearson studium || pv || - ||
|| software engineering || ian sommerville || pearson studium || pv || - ||
|| freeBSD || -|| franzis || pv || - ||
