---
date: '2016-08-01 21:47:00'
moinmoin_path: /Kalliope/revisions/00000067
title: Kalliope
---
# Kalliope :: Computer und Computerkurse für Flüchtlinge


## Computer courses (not only) for newcomers

**Homepage mit den Kursen und Infos: http://kurs.valug.at**


 * Beginning with january 2016 we do bi-weekly computer courses in Wels
 * We will show you how to use PCs and Laptops with XUBUNTU Linux, this is the system we give away
  . with our hardware (we collect and repair computers for refugees. need a PC? come to a course or write us at kalliope@valug.at)
 * in the future we will show some more -in depth knowledge- like programming and some fun-stuff (physical computing e.g.)

 . ---


 * Beginnend mit Jänner 2016 werden alle 2 Wochen Computerkurse in Wels stattfinden
 * Zu Beginn werden wir vor allem den Umgang mit XUBUNTU Linux thematisieren. Linux ist das System welches wir
  . mit unserer Hardware für Flüchtlinge ausliefern (wir sammeln und reparieren Computer für Flüchtlinge. Sie benötigen einen PC?
  . kommen Sie zu einem der Kurse oder kontaktieren Sie und unter kalliope@valug.at)
 * in Zukunft werden die Kurse auch fortgeschrittenere Themen behandeln wie zB Programmierung oder auch Unterhaltsameres wie "Physical Computing".

 . -----------------------------------------------------------------------------------

# KURSE DATUM :: COURSES DATES

 . WHEN: MITTWOCHS, alle 2 WOCHEN - ALWAYS WEDNESDAY EVERY 2 WEEKS - 18.00h
 . WHERE: ALTER SCHLACHTOF WELS (4600 Wels, Dragonerstrasse 22)
 . wenn möglich, Laptops mitbringen - If possible, bring your Laptops
 * Flyer: [attachment:computer-kurse.pdf](/files/computer-kurse.pdf)
 . ---

 * 13.1.  * 27.1.  * 10.2.  * 24.2.  * 9.3.  * 23.3.  * 6.4.  * 20.4.  * 4.5.
 * 18.5.  * 1.6.  * 15.6.  * 26.6.  * to be continued...
 . For Topics see http://kurs.valug.at

 . --------------------

## Computer sammeln und reparieren


 * Hardware sammeln und diese zu funktionierenden Computern wieder
 * zu recyclen, Gnu/Linux installieren mit allen Programmen die Sinn machen
 * Lernsoftware für Jung und Alt, Office-Software, Internet-Software, Spiele/Unterhaltung
 

 * WAS WIR GERADE DRINGEND SUCHEN: /KalliopeGesucht


![attachment:IMG_20150829_103954.jpg](/files/IMG_20150829_103954.jpg) 

![attachment:landwied.jpg](/files/landwied.jpg) 

## TALK at Linux Presentation Day 2015
 * Attila and Peter did a short talk about the project at LPD15 in Wels:
  * [attachment:Project-Kalliope.pdf](/files/Project-Kalliope.pdf)

## und wo kommen die Computer, Mobiles, Laptops dann so hin?

 * Infrastruktur Kurse Wels (http://kurs.valug.at)
 * Infrastruktur, Laptops + PCs Asyl-/Gästehaus Pettenbach
 * PCs + Laptops "Daheim in Vorchdorf" (2 Häuser)
 * Laptops + PCs Gäste-/Asylhaus Putzleinsdorf
 * PCs Volksschule Vorchdorf
 * Laptops Haus Courage Wels
 * PCs + Laptops Grossendorf
 * Laptops Rüstorf Flüchtlingshilfe
 * Infrastruktur, Laptops + PCs Volkshilfe Haus Wels Lichtenegg
 * Laptops + PCs Caritas Haus Kreuzpointstrasse
 * Laptops + PCs Gäste-/Asylhaus Sattledt



## Computer-Kurs
 * http://kurs.valug.at
  * und natürlich "live" im Alten Schlachthof Wels

## Sprach-Kurs
 * http://kurs.kalliope.at/

## Unterstützer/innen

 * folgende Menschen und Organisationen unterstützen uns:
 
 * valug.at
 * natürlich das urspüngliche Kalliope Projekt http://www.kalliope.at
 * proserver1.at https://www.proserver1.at
 * Funkfeuer Wels http://wels.funkfeuer.at
 * Tabakfabrik Linz http://www.tabakfabrik-linz.at
 * Schweitzer - die ladenmanufaktur http://www.schweitzer.at
 * Hoftaverne Ziegelböck http://www.hoftaverne.at
 * Familie Ablinger - KW Sattelmühle
 * Didi Aschauer http://vinyl.ne.tz
 * Alter Schlachthof Wels http://www.schlachthofwels.at
 * Michael Fekete
 * Brunner Webhosting http://www.brunner.at
 * Webagentur Körbler http://www.koerbler.com
 * Allianz für Kinder http://allianzfuerkinder.at/
 * agrarDATA http://www.agrardata.at
 * Lupo
 * Kunstuniversität Linz http://www.ufg.ac.at/
 * Notizblock http://nblock.org/
 * devLOL https://devlol.org/
 * Anna & Firma
 * Zauberfisch
 * Architekt Siegel Linz http://arch-siegel.at/
 * Schweitzlingers http://daspack.at/
 * Heidi
 * Landwirtschaftskammer Oberösterreich https://ooe.lko.at/
 * Easyname https://www.easyname.at/de/domain/ssl-zertifikat
 * Bundesrealgymnasium Landwiedstraße http://landwied.at/index.html
 * Helfer/innen von Daheim in Vorchdorf http://daheiminvorchdorf.at
 * Mitarbeiter/innen des Ars Elektronica Centers http://aec.at
 * Harri
 * Atlia
 * Hackl Wolfgang http://hackwolf.at
 * Mohsen
 * Masoud
 * Alexander Haas
 * Teufelberger Wels http://teufelberger.com/
 * Facultas Buchhandlung / Verlag Wien http://www.facultas.at/facultas
 * Dietmar
 * Alex
 * Melanie
 * Education Group https://www.edugroup.at/
 * Manuel Schickmaier

 * Besonderen Dank an  http://kalliope.at/ Wien, die "damals" Ideen-Spender waren und immer noch die Sprach-Seiten betreiben
