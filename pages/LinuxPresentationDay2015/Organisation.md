---
date: '2015-11-07 15:45:31'
moinmoin_path: /LinuxPresentationDay2015(2f)Organisation/revisions/00000015
title: Organisation
---
# Linux Presentation Day 2015 Organisation

## Zeitraum
 * Datum ist fix: 14.11.2015
 * Vorschlag von silwol für Uhrzeit: 09:00 bis 17:00

## Aktivitäten

## Fix
 * Alte Computer wiederbeleben / Kalliope (pv, silwol, notizblock).
 * Mediacenter mit Raspberry PI (silwol, notizblock).
 * Computer für Kinder: Spiele, Lernprogramme, Zeichenprogramme (silwol)

## In Überlegung
 * Computerspiele.
 * Funkfeuer.
 * Computer Club Wels.
 * Kurzräsentation über die verschiedenen lokalen LUGs (VALUG/LUGL/fhLUG, …)?
 * Windows-Software unter Linux laufen lassen.

## Vorträge
 * Projekt Kalliope - Gespendete Computer für Flüchtlinge instand setzen
 * Software-Lizenzen (silwol)
 * Die Organisationen (VALUG, Funkfeuer, CCWels) stellen sich vor

## TODO
 * Termin in die Veranstaltungsseiten der Lokalpresse bringen.
 * Lokalpresse informieren mittels Presseaussendung. Notizblock hat alle Pressekontakte von den lokalen Zeitungen aufgetrieben.

## Drucksachen
 * ![Plakat_A3](../plakat_A3.svg)

## Vielleicht noch hilfreich, falls sich wer findet
 * Den [VALUG-Infofolder](/pages/Öffentlichkeitsarbeit) aktualisieren und vor Ort auflegen
 * Plakate in den Geschäften aufhängen (v.a. Fußgängerzone Wels)
