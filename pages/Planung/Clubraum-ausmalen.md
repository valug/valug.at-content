---
date: '2010-07-18 08:38:54'
moinmoin_path: /Planung(2f)Clubraum(20)ausmalen/revisions/00000012
title: Clubraum ausmalen
---
# Planung für Ausmalen
Freitag, 23. Juli 2010 nachmittags

## Benötigt
 * Helfer/innen im Arbeitsgewand
 * Innendispersionsfarbe
 * Farbroller
 * Abdeckplane

## Was wollen wir machen?
 * Möbel rausschleppen
 * Alles fix eingebaute abdecken
 * Ausmalen
 * Ein wenig trocknen lassen
 * Möbel wieder reinschleppen und vorläufig in der Mitte des Raumes stehen lassen

## Helfer
 * [Eraldo](/Eraldo)
   Ich kann mit anpacken, brauche aber eine Mitfahrgelegenheit von Linz aus.
 * [notizblock](/notizblock)
   Bei mir isses noch nicht fix, aber im Fall kann ich Linzer mitnehmen.
 * [silwol](/silwol)
   fix
