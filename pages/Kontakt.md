---
date: '2019-05-07 12:36:55'
moinmoin_path: /Kontakt/revisions/00000007
title: Kontakt
---
# Kontakt

Da die VALUG keine Organisation im herkömmlichen Sinne ist, gibt es auch nicht ''die'' Kontaktadresse. Über folgende Möglichkeiten kannst du Kontakt mit VALUG-Mitgliedern aufnehmen:
 * [Chat](/pages/Chat)
 * [Mailingliste](/pages/Mailingliste)
 * [Fediverse/Mastodon: @valug@floss.social](https://floss.social/@valug)
 * Besuche uns bei einer der Veranstaltungen, die regelmäßig auf der [Startseite](/) angekündigt werden.
