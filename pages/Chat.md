---
date: '2021-06-13 11:18:51'
moinmoin_path: /Chat/revisions/00000012
title: Chat
---
{{% comment %}} page was renamed from IRC{{% /comment %}}
# Chat

Die VALUG ist auch im IRC- und im Matrix-Chat vertreten. Dort koennt ihr eure Fragen, Vorschlaege etc. direkt an die Community richten. Der VALUG Channel ist ein freundlicher deutschsprachiger Tech-Channel, gegen ein paar Runden OT(Off-Topic)-Talk ist natuerlich auch nichts einzuwenden.

Der Chatraum ist in zwei Technologien verfügbar, diese sind jedoch miteinander verknüpft, sodass es prinzipiell egal ist, welche genützt wird. Es ist immer möglich mit allen Teilnehmern auf beiden Technologien zu kommunizieren.

Matrix-Channel: [#valug:matrix.org](https://matrix.to/#/#valug:matrix.org)

IRC-Server: [irc.libera.chat](irc://irc.libera.chat/)

IRC-Channel: [#valug](irc://irc.libera.chat/#valug)

## Was ist Matrix Chat?
Matrix Chat ist ein offenes Protokoll für Chat. Die Funktionalität ähnelt dem, was von Whatsapp oder Signal bekannt ist. Im Gegensatz zu diesen Applikationen ist Matrix jedoch, ähnlich wie E-Mail, dezentral organisiert, und daher nicht vom Gutwill einer einzelnen Organisation abhängig.

Matrix kann sowohl öffentlich einsehbar im Klartext, als auch verschlüsselt verwendet werden. Der [VALUG-Chatroom](https://matrix.to/#/#valug:matrix.org) ist öffentlich und unverschlüsselt, sodass alle mit möglichst niedriger Hürde direkt teilnehmen können. Der Raum ist auch für Gäste offen, sodass keine Registrierung bei einem Matrix-Provider notwendig ist.

## Was ist IRC?
Internet Relay Chat ist ein textbasiertes Echtzeit-Kommunikationsmedium. Das heißt, was man reinschreibt, können alle anderen Teilnehmer des Kanals unmittelbar danach lesen. Die meisten von euch werden diese Art der Kommunikation einfach unter dem Namen **Chat** kennen.

## Wie nehme ich teil?
Man benötigt einen Matrix- oder IRC-Client (also ein Chat-Programm).

Matrix-Programme sind unter [https://matrix.to/#/#valug:matrix.org](https://matrix.to/#/#valug:matrix.org) zu finden.

Verbreitete IRC-Programme sind:
 * [Empathy](http://live.gnome.org/Empathy), standardmäßig in Ubuntu mit an Bord
 * [Konversation](http://konversation.kde.org/)
 * [Pidgin](http://pidgin.im/) (auch für Windows verfügbar)
 * [XChat](http://xchat.org/)
 * [Irssi](http://irssi.org/) (Konsolenprogramm)

## Hilfestellung für den IRC
Folgende Links allen IRC Newbies/Oldies den Umgang bzw. ersten Kontakt mit dem Chat etwas erleichtern:

 * [http://irchelp.org](http://irchelp.org)
 * [http://srcery.net/help](http://srcery.net/help)
 * [http://irc.cg.yu](http://irc.cg.yu)

Für Einsteiger könnte ein [einführender_Artikel_bei_LinuxReviews](http://linuxreviews.org/software/irc/index.html.en) interessant sein.

## Historisches

Leider musste der VALUG-Channel auf matrix.org 2021-06-13 auf einen neuen Channel umgezogen werden. Dieser ist nach wie vor unter der gleichen Adresse [#valug:matrix.org](https://matrix.to/#/#valug:matrix.org) erreichbar. Wer die Konversationen im alten Channel nachlesen möchte, findet sie im Channel [#valug-old:matrix.org](https://matrix.to/#/#valug-old:matrix.org). Die technischen Probleme hatten mit der IRC-Bridge und dem Umzug von Freenode zu Libera.chat zu tun. Die Bugs die uns getroffen haben sind https://github.com/matrix-org/matrix-appservice-irc/issues/1324 und https://github.com/matrix-org/matrix-appservice-irc/issues/1323 welche einen stale appservice-irc bot bewirkten, den wir nicht mehr losbekommen haben.
