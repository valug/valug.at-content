---
date: '2011-09-05 11:42:16'
moinmoin_path: /Bibliothek/revisions/00000005
title: Bibliothek
---
# Bibliothek

![drawing:1.png](/files/1.png.png)

![drawing:2.png](/files/2.png.png)

Die Bibliothek im [Klubraum](/pages/Klubraum) beinhaltet Bücher und Zeitschriften, die von den Mitgliedern gratis zur Verfügung gestellt werden. Eine Liste der Bücher findet sich unter [Bücher](/pages/Bibliothek/Bücher).

## Ausleihen
In jedem Buch liegt die Verleihliste. Wer das Buch ausleiht, trägt seinen (Nick-)Namen und das Ausleihdatum in der jeweiligen Verleihliste ein und gibt sie in die Plastikbox. Bei der Rückgabe wird der Eintrag durchgestrichen und die Liste wieder in das Buch gelegt.
