---
date: '2016-01-11 10:54:36'
moinmoin_path: /Kalliope(2f)KalliopeGesucht/revisions/00000007
title: KalliopeGesucht
---
# Kalliope: Computer sammeln und reparieren

 * Hardware sammeln und diese zu funktionierenden Computern wieder recyclen

**WAS WIR GERADE DRINGEND SUCHEN: **

 * Die Lager sind leer! (man glaubt es kaum...)
 * also gesucht wird -> alles .-)



** was wir generell sammeln: **

 * Geldspenden (damit wir gezielt "Kleinteile" nachkaufen können)
 * Laptops (möglichst grosse Stückzahlen zB aus Firmen)
 * Komplette PCs (möglichst grosse Stückzahlen zB aus Firmen)
 * SATA-Festplatten (viele PCs bekommen wir ohne Festplatetn)
 * Computermonitore LCD
 * Tastaturen und Mäuse
 * Laptop-Netzteile
 * Strom Verlängerungskabel und Verteiler
 * Handy-Wertkarten und SIMs
