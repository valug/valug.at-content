---
date: '2012-01-22 20:55:23'
moinmoin_path: /Liwoli2012/revisions/00000007
title: Liwoli2012
---
# Planungen für VALUG-Aktivitäten auf den Linuxwochen Linz 2012

 * **Datum:** 24. bis 26. Mai
 * **Uhrzeit:** noch unbekannt
 * **Ort:** Kunstuni, 1 Raum für VALUG zur freien Verfügung reserviert

## Tagesprogramm
 * Mik: vmcluster
 * silwol: openstreetmap (1x für potentielle Anwender/innen, 1x für aktive Mapper/innen, bin bei Slot-Knappheit jedoch gerne bereit auf eines davon zu reduzieren)
 * laxity: ChucK (vielleicht)
 * mariok: gnupg
 * notizblock, silwol: Einstieg in git
 * silwol, [hoffentlich noch ein paar VALUGer/innen]: Vorstellung der VALUG, Bericht über unsere Aktivitäten?
 * **weitere Vorträge / Workshops aus dem VALUG-Umfeld (tragt euch bitte hier ein)**
## Abendprogramm
 * **24.5.** Videoabend (silwol)
 * **25.5.** Creative-Commons Music-Abend (oder Nacht?) (laxity)
## Sonstiges Rahmenprogramm
 * Kinder-PCs mit GCompris und Tuxpaint aufstellen
 * OpenMate ausschenken
## Freiwillige zur Begleitung des Rahmenprogramms
 * silwol
 * laxity
 * **weitere VALUG-Members (tragt euch bitte hier ein)**
## Camping im Anschluss für VALUGer/innen
 * Termin: 26. abends (nach Aufräumen in Linz) bis 29. Mai (+/- 1 Tag)
 * Ort: entweder ein Campingplatz oder silwol@home
 * Ohne Programm
