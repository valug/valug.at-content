---
date: '2021-08-07 17:16:38'
moinmoin_path: /Mailingliste/revisions/00000008
title: Mailingliste
---
# Mailingliste

 * [Anmeldung](https://mailman.proserver1.at/postorius/lists/liste.lists.valug.at/)
 * U.a. um der DSGVO zu entsprechen, führen wir keine Archive mehr
