---
date: '2022-06-11 08:03:38'
moinmoin_path: /Index(2f)Vergangene(/events/20)Veranstaltungen/revisions/00000096
title: Vergangene Veranstaltungen
---
# Vergangene Veranstaltungen

|**Thema**|**Datum**|**Anzahl Personen**|
|---|---|---|
|[WireGuard, Headscale und Tailscale](/events/2022-06-11-tailscale)|2022-06-10| |
|…|…|…|
|[Das Fediverse](/events/2020-04-10-Das-Fediverse)|2020-04-10| |
|…|…|…|
|[Debian Buster Release Party](/events/2019-07-12-Debian-Buster-Release-Party)|2019-07-12| |
|Linux Device Tree|2019-06-14|4|
|[Kaffee Rösterei Dunkelhell](/events/2019-05-10-Kaffee-Rösterei-Dunkelhell)|2019-05-10|17|
|[Taskwarrior](/events/2019-04-12-Taskwarrior)|2019-04-12|6|
|[LAN-Party](/events/2019-03-08-LAN-Party)|2019-03-08|10|
|[The Go Programming Language](/events/2019-02-08-golang)|2019-02-08|9|
|[Games](/events/2019-01-11-Games)|2019-01-11|8|
|[Tools](/events/2018-12-14-Tools)|2018-12-14|6|
|Besuch bei Team14 (Grillen, OpenPNP) |2018-05-01|11|
|Show & Tell: 3D-Drucker |2018-03-09| |
|VALUG Treffen|2018-04-14| |
|VALUG Treffen|2018-02-09|7|
|VALUG Treffen|2018-01-12|13|
|VALUG Treffen|2017-12-08|1|
|VALUG Treffen mit Minetest spielen|2017-11-17|10|
|[ENGICAM Single Board Computer](/events/2017-06-09-ENGICAM-Single-Board-Computer)|2017-06-09| 9 |
|[Ein Blick auf SSH](/events/2017-04-14-Ein-Blick-auf-SSH)|2017-04-14| 15 |
|Configuration Management & DevOps|2017-02-10|10|
|[Matrix Chat](/events/2017-01-13-Matrix-Chat)|2017-01-13|14|
|[KiCad](/events/2016-12-09-KiCad)|2016-12-09| 8 |
|[IPFS](/events/2016-09-09-IPFS)|2016-09-09| 10 |
|[Tools](/events/2016-08-12-Tools)|2016-08-12|8|
|VALUG Workshop - Tryton ERP|2016-07-15|11|
|[Webscraping mit Scrapy](/events/2016-05-13-Webscraping-mit-Scrapy)|2016-05-13|9|
|[One Time Passwords](/events/2016-04-08-One-Time-Passwords)|2016-04-08|13|
|[VALUG Stammtisch mit Vortrag Die Godot Spieleengine](/events/2016-03-11-Die-Godot-Spieleengine)|2016-03-11|12|
|VALUG Stammtisch|2016-02-12|10|
|VALUG Stammtisch mit Vortrag Antennentechnik|2016-01-08|10|
|Weihnachtsfeier|2015-12-11|10|
|[Linux Presentation Day 2015](/pages/LinuxPresentationDay2015)|2015-11-14| |
|Vorbereitungen Linux Presentation Day 2015|2015-11-13|8|
|Diskussion über unsere Infrastruktur|2015-10-09|9|
|Kalliope Aktionstag|2015-09-11|10|
|[Personal Information Management](/events/2015-08-14-Personal-Information-Management)|2015-08-14|11|
|3D-Grafik mit Blender|2015-07-10|12|
|Wir spielen Mutter, Vater, Kind: Stammbaumverwaltung mit Gramps|2015-06-12|9|
|Show & tell vor Ort: Softwareentwicklung & Infrastruktur mit freier Software in einem Startup mit anschließender Grillerei|2015-05-08|19|
|Show & tell: Entwicklung einer Webapplikation mit Python, Flask & !SqlAlchemy (von notizblock) und Laptops für Kalliope installieren|2015-04-10|8|
|Die Programmiersprache Rust (von flo)|2015-03-13| 9 |
|Auf dem Laufenden bleiben mit TinyTinyRSS (von notizblock)|2015-02-13|9|
|Kino: Citizen Four|2015-01-09|9|
|Weihnachtsfeier|2014-12-12|11|
|[Vortrag + Diskussion: Sporttracker unter Linux/Android](/events/2014-11-14-sporttracking)|2014-11-14|8|
|Vortrag + Diskussion: Privatsphäre und Sicherheit bei Benutzung des Internets und auch mobiler Kommunikation.|2014-10-10|10|
|Vortrag + Diskussion: Nachhaltig "garteln" und der Bezug zu Freier Software|2014-09-12|9|
|Android rooting & ROMing -- Vorteile, Gefahren, Hilfe zur Selbsthilfe|2014-07-11|9|
|Festplattenverschlüsselung unter GNU/Linux|2014-04-11|10|
|WLAN Standard 802.11|2014-03-14|5|
|[ownCloud](/events/2014-02-14-ownCloud)|2014-02-14|11|
|[btrfs](/events/2013-12-13-btrfs)|2013-12-13|16|
|VALUG Stammtisch bei der FSFE Fellowship Group Linz|2013-11-22|5|
|VALUG Stammtisch|2013-10-11|7|
|VALUG Stammtisch|2013-09-13|10|
|VALUG-Stammtisch|2013-08-09|10|
|[Finanzverwaltung mit freier Software (GnuCash)](/events/2013-07-12-Finanzverwaltung-mit-freier-Software)|2013-07-12| 10 |
|Spiele unter GNU/Linux|2013-06-14| 10 |
|!GnuCash|2013-05-10|15|
|C64|2013-04-12| 15 |
|GnuPG und !SmartCards|2013-03-08|14|
| |2013-02-08| 8 |
|[Vortrag: SHA1 und OpenPGP/GnuPG](/events/2013-01-11-Vortrag-SHA1-und-OpenPGPGnuPG)|2013-01-11| 10 |
|Exkursion: Brauereiführung Gerstl Bräu|2012-12-14|8|
|[Workshop: Lizenzdschungel](/events/2012-11-16-Workshop-Lizenzdschungel)|2012-11-16|10|
|[Tor](/events/2012-10-12-Tor)|2012-10-12|12|
| |2012-09-19|16|
|Workshop: Backups done right|2012-09-14|5|
|[Workshop: Participating in Free Software Projects](/events/2012-08-10-Workshop-Participating-in-Free-Software-Projects)|2012-08-10|13|
|[10-Jahresfeier](/events/2012-07-21-10-Jahresfeier)|2012-07-21|16|
|VALUG-Stammtisch|2012-07-13|5|
|Workshop: IPv6|2012-06-15|11|
|Camp 2012 Nachbesprechung|2012-06-08|6|
|VALUG bei den Linuxwochen in Linz|2012-05-24| |
|VALUG bei den Linuxwochen in Linz|2012-05-25| |
|VALUG bei den Linuxwochen in Linz|2012-05-26| |
|[Deaddrops installieren](/events/2012-05-11-Deaddrops-installieren)|2012-05-11|6|
|VALUG-Stammtisch|2012-04-13|9|
|!RepRap|2012-03-09|14|
|Videoabend !PioneerOne|2012-02-10|10|
|!OpenStreetMap Night of the Living Maps|2012-02-07| |
|VALUG-Stammtisch|2012-01-20|4|
|CACert- und GPG-Keysigning-Party|2012-01-13|17|
|VALUG-Stammtisch|2011-12-09|11|
|VALUG-Stammtisch|2011-11-11|9|
|[Besprechung für Camp-Organisation](/events/2011-10-14-Besprechung-für-Camp-Organisation-2012)|2011-10-14|10|
|VALUG-Stammtisch|2011-09-09|11|
|VALUG-Stammtisch|2011-08-12|3|
|VALUG-Stammtisch|2011-07-08|5|
|VALUG-Stammtisch|2011-06-10|7|
|VALUG-Stammtisch|2011-05-13|3|
|[Einführung in die verteilte Versionskontrolle unter Git](/events/2011-04-08-Einführung-in-die-verteilte-Versionskontrolle-unter-Git)|2011-04-08|5|
|[CSharp und GTK](/events/2011-03-11-CSharp-und-GTK)|2011-03-11|7|
|Videoabend !PioneerOne|2011-03-04|6|
|VALUG-Stammtisch|2011-02-11|6|
|VALUG-Stammtisch|2011-01-14|6|
|[OpenTTD-LAN](/events/2011-01-07-OpenTTD-LAN)|2011-01-07|9|
|VALUG-Stammtisch|2010-12-10|6|
|!OpenStreetMap Mapping in Wels|2010-11-12|4|
|[Python Einführung](/events/2010-10-08-Python-Einführung)|2010-10-08|7|
|VALUG-Stammtisch|2010-09-10|6|
|VALUG-Clubraum neugestalten|2010-08-13|7|
|VALUG-Clubraum ausmalen|2010-07-23|5|
| |2010-07-09|7|
| |2010-06-11|7|
| |2010-05-14|8|
| |2010-04-09|6|
| |2010-03-12|5|
| |2010-02-12|9|
| |2010-01-08|8|
| |2009-12-11|6|
| |2009-11-13|11|
| |2009-10-09|11|
| |2009-09-11|13|
| |2009-06-26|6|
| |2009-06-12|10|
| |2009-05-08|9|
| |2009-03-13|7|
| |2009-02-13|7|
| |2009-01-09|7|
| |2008-12-19|9|
| |2008-11-14|6|
| |2008-10-10|5|
| |2008-09-12|10|
| |2008-08-08|5|
| |2008-06-13|8|
| |2008-04-11|9|
| |2008-03-14|4|
| |2008-01-11|6|
| |2007-12-14|10|
| |2007-10-12|5|
