---
date: '2015-05-26 20:29:30'
moinmoin_path: /Index(2f)Vergangene(20)Veranstaltungen(2f)2013(2d)12(2d)13(20)btrfs/revisions/00000004
title: 2013-12-13 btrfs
start: '2013-12-13 18:30:00'
location: VALUG-Klubraum im Alten Schl8hof Wels
---
# btrfs - Eine Einführung
 * **Datum**: Freitag, 13. Dezember 2013 18:30
 * **Ort**: VALUG-Klubraum im Alten Schl8hof Wels
 * **Details**: Vortrag von notizblock

Inhalt:
 * Was ist btrfs?
 * Vorstellung von ausgewählten Features
 * Experimentieren mit ausgewählten Features
 * Diskussion

## Slides
 * **Git-Repository:** https://gitlab.com/valug/notizblock-btrfs-slides/tree/valug-20131213
 * **PDF:** [Download](http://nblock.org/static/talks/slides-btrfs-valug-20131213.pdf)
