---
date: '2010-10-08 19:04:55'
moinmoin_path: /Index(2f)Vergangene(20)Veranstaltungen(2f)2010(2d)10(2d)08(20)Python(20)Einf(c3bc)hrung/revisions/00000003
title: "2010-10-08 Python Einf\xFChrung"
start: '2010-10-08 18:30:00'
---
# Python Einführung
Vortragender: Albert Brandl

## Daten
 * [Vortragsfolien_(PDF)](../vortrag.pdf)
 * [Source-Dateien_(.tar.gz)](../Daten.tar.gz)
