---
date: '2012-01-12 00:49:34'
moinmoin_path: /Index(2f)Veranstaltungen(2f)2012(2d)01(2d)13(2d)Keysigning/revisions/00000012
title: 2012-01-13-Keysigning
start: '2012-01-13 18:30:00'
location: VALUG-Klubraum im Alten Schl8hof Wels
---
# VALUG CAcert-Assuring und GPG-Keysigning-Party am 13.01.2012

 * **Datum**: 13.01.2012
 * **Uhrzeit**: 18:30
 * **Ort**: [Klubraum](/Klubraum)

Wir veranstalten dieses Treffen, um das [Web_of_Trust](http://de.wikipedia.org/wiki/Web_of_Trust) auch im Raum Wels etwas zu erweitern.

Wer seine Identität gegenüber CAcert bestätigen oder seine GPG-Keys signiert haben will, muss zwei gültige Lichtbildausweise mitnehmen: Reisepass, Führerschein oder den staatlichen Personalausweis.

## GPG-Keysigning
Grundsätzliche Informationen zur Funktionsweise von GPG (GnuPG) finden sich auf der [GnuPG-Webseite](http://gnupg.org/howtos/de/index.html).
Die Keysigning Party wird nach [Len_Sassamans_Efficient_Group_Key_Signing_Model](http://keysigning.org/methods/sassaman-efficient) abgehalten.

Wer seine Schlüssel signieren lassen will, muss sie bis **Mittwoch, 11. Jänner 2012 - 23:59** an unseren Schlüsselserver schicken.

<code>% gpg --keyserver hkp://ksp.valug.at --send YOURKEYID</code>

wobei sich <code>YOURKEYID</code> mittels

<code>% gpg --list-secret-keys</code>

herausfinden lässt. Es ist der Buchstabenblock nach dem Schrägstrich in den Zeilen, die mit <code>sec</code> beginnt, also im Beispiel "**<code>sec   1024D/540EDE7A 2007-08-15</code>**" die Buchstabenfolge **<code>540EDE7A</code>**.

Unter [http://ksp.valug.at/keys/](http://ksp.valug.at/keys/) sollte danach der Schlüssel aufscheinen.

Bis Freitag, 13. Jänner kann man dann den kompletten Schlüsselbund und eine Textdatei ([ksp-valug12.txt](http://ksp.valug.at/ksp-valug12.txt)) mit allen Schlüsseln herunterladen.
Danach überprüft man ob sein eigener Schlüssel in [ksp-valug12.txt](http://ksp.valug.at/ksp-valug12.txt) korrekt ist und berechnet davon den SHA256 und den RIPEMD160 Hash.

**Die endgültigen Dateien:**
 * [Teilnehmerliste_ksp-valug12.txt](http://ksp.valug.at/ksp-valug12.txt)
 * [Schlüsselbund_ksp-valug12.asc](http://ksp.valug.at/ksp-valug12.asc)
RIPEMD160 Hash beginnt mit: BED7 F130 CC7D .... ....

<code>% gpg --print-md ripemd160 ksp-valug12.txt</code>

<code>% gpg --print-md sha256 ksp-valug12.txt</code>

Die Datei wird danach ausgedruckt und man füllt die berechneten Checksummen auf dem Ausdruck ein.
Beim VALUG Treffen werden dann die Checksummen und Ausweise verglichen und anschliessend kann jeder die geprüften Schlüssel signieren und demjenigen zukommen lassen.


## CAcert-Assurance
Informationen zu CAcert gibt es auf der [CAcert-Webseite](http://cacert.org/) und im [CAcert-Wiki](http://wiki.cacert.org/FAQ/AboutUs).

Wir versuchen möglichst viele CAcert-Assurer zum Treffen einzuladen, damit ihr auch möglichst viele Punkte auf ein Mal abholen könnt.

Wer seine Identität gegenüber CAcert bestätigen lassen möchte, muss sich zuvor einen [CAcert-Account_anlegen](https://www.cacert.org/index.php?id=1) und sich merken, welche E-Mail-Adresse zur Anmeldung verwendet wurde.
