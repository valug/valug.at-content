---
date: '2015-08-15 21:34:33'
moinmoin_path: /Index(2f)Vergangene(20)Veranstaltungen(2f)2015(2d)08(2d)14(20)Personal(20)Information(20)Management/revisions/00000001
title: 2015-08-14 Personal Information Management
start: '2015-08-14 18:30:00'
---
# Personal Information Management
Stammtisch vom 14.8.2015

## Linksammlung
 * https://github.com/strukturag/spreed-webrtc
 * https://owncloud.org/
 * http://radicale.org/
 * http://baikal-server.com/
 * https://davdroid.bitfire.at/
 * https://community.zarafa.com/
 * http://kolab.org/
