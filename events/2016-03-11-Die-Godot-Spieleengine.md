---
date: '2016-03-18 15:45:29'
moinmoin_path: /Index(2f)Vergangene(20)Veranstaltungen(2f)2016(2d)03(2d)11(20)Die(20)Godot(20)Spieleengine/revisions/00000003
title: 2016-03-11 Die Godot Spieleengine
start: '2016-03-11 18:30:00'
---
# Die Godot Spieleengine here
2016-03-11

## Linksammlung
 * [Offizielle_Webseite](http://godotengine.org/)
 * [Spiele-Engine_Godot_in_Version_2.0_freigegeben_(Pro-Linux)](http://www.pro-linux.de/news/1/23284/spiele-engine-godot-in-version-20-freigegeben.html)

## Weiterführende Infos
 * Eine weitere Game Engine mit ähnlicher Funktionsweise wurde im Quellcode freigegeben: [Atomic_Game_Engine](http://atomicgameengine.com/blog/announcement-2/)
