---
date: '2016-08-15 09:50:58'
moinmoin_path: /Index(2f)Vergangene(20)Veranstaltungen(2f)2016(2d)08(2d)12(20)Tools/revisions/00000023
title: 2016-08-12 Tools
start: '2016-08-12 18:30:00'
---
# Tools
2016-08-12

Wir haben uns getroffen und über verschiedene Tools für die unterschiedlichsten Zwecke gesprochen. Hier eine unvollständige Liste der besprochen Programme:

 * [Ddrescue](https://www.gnu.org/software/ddrescue/): Blockweise Recovery-Kopie von Disk-Images, mit gezielten mehrfachen Leseversuchen bei Fehlschlägen
 * [PhotoRec](https://www.cgsecurity.org/wiki/PhotoRec): Recovery von Bildern und anderen Dateien
 * [TestDisk](https://www.cgsecurity.org/wiki/TestDisk): Recovery von Dateisystemen
 * [urlwatch](https://thp.io/2008/urlwatch/): Veränderungen auf HTTP-URLs beobachten
 * [Mandos](https://wiki.recompile.se/wiki/Mandos): Unattended boot von verschlüsselten Rechnern
 * [BRISS](http://briss.sourceforge.net/): Zurechtschneiden von beschriebenen Bereichen in PDF-Datein
 * [FireFox](https://www.mozilla.org/firefox): Browser
  * [UBlock](https://www.ublock.org/): Adblock
  * [Ghostery](https://addons.mozilla.org/en-US/firefox/addon/ghostery/): Tracker-Blocker
  * [FireGestures](https://addons.mozilla.org/en-US/firefox/addon/firegestures/): Maus-Gesten
 * [FireFox](https://www.mozilla.org/firefox): Browser
 * [uzbl](http://www.uzbl.org/): Uzbl ist ein freier minimalistischer Webbrowser für Unix-ähnliche Betriebssysteme
 * [EasyStroke](https://github.com/thjaeger/easystroke/wiki): Maus-Gesten für X11
 * [CMUS](https://cmus.github.io/): Musik-Player auf der Konsole
 * [qTox](https://tox.chat/): P2P-Realtime-Kommunikation
 * [ring](https://ring.cx/): P2P-Realtime-Kommunikation
 * [Briar](https://briarproject.org/): P2P-Realtime-Kommunikation
 * [Matrix](https://matrix.org/) / [Vector](https://vector.im/): Dezentrale Realtime-Kommunikation
 * [Conversations](https://conversations.im/): Android Jabber-Client mit OMEMO/OLM Verschlüsselungsunterstützung
 * [TaskWarrior](https://taskwarrior.org/): Fortgeschrittene Tasks-Verwaltung
 * [Org_mode](http://orgmode.org/): Fortgeschrittene Tasks-Verwaltung in Emacs
 * [Radicale](http://radicale.org/): CalDAV und CardDAV Server
 * [DAVdroid](https://davdroid.bitfire.at/): CalDAV und CardDAV Android-Synchronisation
 * [Geany](https://www.geany.org/): GTK-basierter Editor
 * [VIM](http://www.vim.org/): Tastaturlastiger fortgeschrittener Editor
  * [Unimpaired](http://www.vim.org/scripts/script.php?script_id=1590): "Pairs of handy bracket mappings"
  * [Syntastic](https://github.com/scrooloose/syntastic): Linting tools im VIM einbinden
 * [GNU_Emacs](https://www.gnu.org/software/emacs/): Der andere fortgeschrittene Editor
 * [Qt_Creator](https://www.qt.io/ide/): IDE, hauptsächlich für Qt, aber auch andere Targets
 * [Mutt](http://www.mutt.org/): Email-Client auf der Konsole
 * [Dovecot](http://dovecot.org/): IMAP and POP3 Server
 * [FDM](https://github.com/nicm/fdm): Mail fetcher
 * [VLC](https://www.videolan.org/): VideoLAN VLC Media Player
 * [MPlayer](https://mplayerhq.hu/): Movie Player
 * [Kodi](https://kodi.tv/): Media Center
 * [QuodLibet](http://quodlibet.readthedocs.io/en/latest/): Musik-Player
 * [youtube-dl](http://youtube-dl.org/): Downloader für Videoplattformen
 * [Mediathekview](http://zdfmediathk.sourceforge.net/): Downloader für Mediatheken
 * [Gnucash](https://www.gnucash.org/): Finanzverwaltung
 * [GenealogyJ](http://genj.sourceforge.net/): Genealogie-Programm
 * [Gramps](https://gramps-project.org/): Genealogie-Programm
 * [htop](http://hisham.hm/htop/): Prozess-Viewer für die Commandline
 * [iotop](http://iotop.slax.org/): I/O-Viewer für die Commandline
 * [ssh](http://www.openssh.com/): Remote-Zugriff auf Terminals
 * [GNU_Screen](https://www.gnu.org/software/screen/): Terminal Multiplexer
 * [tmux](https://tmux.github.io/): Teminal Multiplexer
 * [QPS](https://github.com/qtdesktop/qps): Process viewer
 * [ansible](https://www.ansible.com/): Infrastruktur-Automation
 * [puppet](https://puppet.com/): Infrastruktur-Automation
 * [vagrant(up)](https://www.vagrantup.com/): command-line VM management
 * [InfluxDB](https://influxdata.com/): Time series database
 * [Graphite](https://github.com/graphite-project/): Time series database
 * [Grafana](http://grafana.org/): Visualisierung von time series databases (Frontend für Graphite, InfluxDB, …)
 * [Icinga](https://www.icinga.org/): Monitoring
 * [Diamond](https://github.com/python-diamond/Diamond): A python daemon that collects system metrics and publishes them to Graphite
 * [git_source_code_management](https://git-scm.com/)
 * [mr_(myrepo)](https://myrepos.branchable.com/): Mehrere version control repositories parallel verwalten
 * [Gource](http://gource.io/): Beeindruckende DVCS-Visualisierung
 * [Gitlab](https://gitlab.com/gitlab-org/gitlab-ce): Selfhosted Git-Server mit Weboberfläche
 * [Gitlab-Maestro](https://gitlab.com/xell/gitlab-maestro): Simple orchestration for Gitlab servers
 * [Gogs](https://gogs.io/): Go Git Service
 * [Weblate](https://weblate.org/): Web-basiertes Übersetzungsmanagement
 * [Jenkins](https://jenkins.io/): Continuous Integration Server
 * [HexChat](https://hexchat.github.io/): IRC client based on XChat
 * [BackupPC](http://backuppc.sourceforge.net/)
 * [Borg_Backup](https://github.com/borgbackup/borg/)
