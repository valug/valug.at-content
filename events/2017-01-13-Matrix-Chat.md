---
date: '2017-01-13 16:58:54'
moinmoin_path: /Index(2f)Vergangene(20)Veranstaltungen(2f)2017(2d)01(2d)13(20)Matrix(20)Chat/revisions/00000001
title: 2017-01-13 Matrix Chat
start: '2017-01-13 18:30:00'
---
# 2017-01-13 Matrix Chat

 * Slides: [attachment:presentation.html](../presentation.html)
 * Slides-Quelltext: https://gitlab.com/valug/matrix-chat-slides
