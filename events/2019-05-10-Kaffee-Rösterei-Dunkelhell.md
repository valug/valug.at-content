---
date: '2019-05-11 22:27:41'
moinmoin_path: /Index(2f)Vergangene(20)Veranstaltungen(2f)2019(2d)05(2d)10(20)Kaffee(20)R(c3b6)sterei(20)Dunkelhell/revisions/00000001
title: "2019-05-10 Kaffee R\xF6sterei Dunkelhell"
start: '2019-05-10 18:30:00'
---
# 2019-05-10 Kaffee Rösterei Dunkelhell

Wir waren zu Besuch in der Kaffeerösterei Dunkelhell, die Peter betrieben wird. Peter war in der Anfangszeit aktiver VALUG-er, und hat uns in seine Rösterei eingeladen, um seinen DIY-Kafferöster zu begutachten und den Kaffee zu verkosten. Sehr lecker!
