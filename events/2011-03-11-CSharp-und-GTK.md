---
date: '2011-03-11 18:34:55'
moinmoin_path: /Index(2f)Vergangene(20)Veranstaltungen(2f)2011(2d)03(2d)11(20)CSharp(20)und(20)GTK/revisions/00000005
title: 2011-03-11 CSharp und GTK
start: '2011-03-11 18:30:00'
---
# 2011-03-11 CSharp und GTK
 * Slides:
  * [Download_PDF](../slides-gtk-csharp.pdf)
  * [Git](http://gitorious.org/valug/csharp-gtk-workshop)
 * Beispiel-Sourcecode:
  * [Git](http://gitorious.org/valug/rss-reader-demo-csharp)
