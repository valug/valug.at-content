---
date: '2020-04-10 11:19:46'
moinmoin_path: /Index(2f)Vergangene(20)Veranstaltungen(2f)2020(2d)04(2d)10(20)Das(20)Fediverse/revisions/00000003
title: 2020-04-10 Das Fediverse
start: '2020-04-10 18:30:00'
location: https://meet.apronix.net/valug
---
# Online-Stammtisch über das Fediverse 2020-04-10

Online-Meeting mittels Jitsi unter https://meet.apronix.net/valug

## Vortragsfolien
 * [Direkt-Link_für_Download](https://valug.at/Index/Vergangene%20Veranstaltungen/2020-04-10%20Das%20Fediverse?action=AttachFile&do=get&target=VALUG-Vortrag+Fediverse.html)
 * [Attachment_zu_dieser_Seite](/attachment:VALUG-Vortrag Fediverse.html)
