---
date: '2012-08-11 12:24:26'
moinmoin_path: /Index(2f)Vergangene(20)Veranstaltungen(2f)2012(2d)08(2d)10(20)Workshop(3a20)Participating(20)in(20)Free(20)Software(20)Projects/revisions/00000001
title: '2012-08-10 Workshop: Participating in Free Software Projects'
start: '2012-08-10 18:30:00'
location: VALUG-Klubraum im Alten Schl8hof Wels
---
# Workshop: Participating in Free Software Projects
 * **Datum**: Freitag, 10.August 2012 18:30
 * **Ort**: VALUG-Klubraum im Alten Schl8hof Wels
 * **Details**: Workshop mit laxity, notizblock und silwol.

Themen:
 * Community und Projektorganisation
 * Bug reports und Feature requests erstellen
 * Code beitragen
 * Diskussion

## Slides
 * **Git-Repository:** https://gitorious.org/valug/participating-in-free-software-projects-slides
 * **PDF:** [Download](../slides-participating.pdf)
