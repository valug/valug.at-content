---
date: '2011-04-08 21:05:20'
moinmoin_path: /Index(2f)Vergangene(20)Veranstaltungen(2f)2011(2d)04(2d)08(20)Einf(c3bc)hrung(20)in(20)die(20)verteilte(20)Versionskontrolle(20)unter(20)Git/revisions/00000003
title: "2011-04-08 Einf\xFChrung in die verteilte Versionskontrolle unter Git"
start: '2011-04-08 18:30:00'
location: VALUG-Klubraum im Alten Schl8hof Wels
---
# Einstieg in die verteilte Versionskontrolle mit Git
 * **Datum**: Freitag, 08.April 2011 18:30
 * **Ort**: VALUG-Klubraum im Alten Schl8hof Wels
 * **Details**: Workshop mit notizblock und silwol.

Themen:
 * Eigene Git-Repositories anlegen
 * Fremde Git-Repositories klonen
 * Git-Repositories veröffentlichen
 * Verteilte Zusammenarbeit, Zusammenführen von Änderungen aus mehreren Repositories

## Slides
 * **Git-Repository:** https://gitorious.org/valug/git-slides
 * **PDF:** [Download](../slides-git.pdf)
