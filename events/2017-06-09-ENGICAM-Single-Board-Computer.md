---
date: '2017-06-11 14:10:58'
moinmoin_path: /Index(2f)Vergangene(20)Veranstaltungen(2f)2017(2d)06(2d)09(20)ENGICAM(20)Single(20)Board(20)Computer/revisions/00000001
title: 2017-06-09 ENGICAM Single Board Computer
start: '2017-06-09 18:30:00'
---
# ENGICAM - Single Board Computer

Walter Hofstädtler verschaffte uns einen Einblick in die Varianten und die Programmierung der ENGICAM Single Board Computer. 

Präsentationsfolien: http://www.zeilhofer.co.at/wiki/lib/exe/fetch.php?media=engicam_vortrag_20170609.pdf
