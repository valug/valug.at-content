---
date: '2017-05-07 15:16:59'
moinmoin_path: /Index(2f)Vergangene(20)Veranstaltungen(2f)2017(2d)04(2d)14(20)Ein(20)Blick(20)auf(20)SSH/revisions/00000001
title: 2017-04-14 Ein Blick auf SSH
start: '2017-04-14 18:30:00'
location: VALUG-Klubraum im Alten Schl8hof Wels
---
# Ein Blick auf SSH
 * **Datum**: Freitag, 14. April 2017 18:30
 * **Ort**: VALUG-Klubraum im Alten Schl8hof Wels
 * **Details**: Vortrag von notizblock
 * **Folien:** [Download](https://nblock.org/static/talks/slides-valug-ssh.pdf)
