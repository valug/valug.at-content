---
date: '2013-01-13 12:13:40'
moinmoin_path: /Index(2f)Vergangene(20)Veranstaltungen(2f)2013(2d)01(2d)11(20)Vortrag(3a20)SHA1(20)und(20)OpenPGP(2c)GnuPG/revisions/00000003
title: '2013-01-11 Vortrag: SHA1 und OpenPGP,GnuPG'
start: '2013-01-11 18:30:00'
location: VALUG-Klubraum im Alten Schl8hof Wels
---
# Vortrag: SHA1 und OpenPGP,GnuPG
 * **Datum**: Freitag, 11.Jänner 2013 18:30
 * **Ort**: VALUG-Klubraum im Alten Schl8hof Wels
 * **Details**: Vortrag

Themen:
 * Was ist SHA-1?
 * Wo wird SHA-1 im OpenPGP Standard verwendet?
 * Wo wird SHA-1 in GnuPG verwendet?
 * Probleme und Interoperabilität
 * Diskussion
