---
date: '2012-11-17 10:05:38'
moinmoin_path: /Index(2f)Vergangene(20)Veranstaltungen(2f)2012(2d)11(2d)16(20)Workshop(3a20)Lizenzdschungel/revisions/00000001
title: '2012-11-16 Workshop: Lizenzdschungel'
start: '2012-11-16 18:30:00'
location: VALUG-Klubraum im Alten Schl8hof Wels
---
# Workshop: Lizendschungel
 * **Datum**: Freitag, 16.November 2012 18:30
 * **Ort**: VALUG-Klubraum im Alten Schl8hof Wels
 * **Details**: Workshop mit notizblock und silwol.

Themen:
 * Strong Copyleft, Weak Copyleft, Permissive Licenses
 * Welche freie Softwarelizenzen gibt es?
 * Wie unterscheiden sich die Lizenzen voneinander?
 * Welche Lizenz verwendet Projekt X?
 * Diskussion
