---
date: '2016-05-14 11:05:31'
moinmoin_path: /Index(2f)Vergangene(20)Veranstaltungen(2f)2016(2d)05(2d)13(20)Webscraping(20)mit(20)Scrapy/revisions/00000003
title: 2016-05-13 Webscraping mit Scrapy
start: '2016-05-13 18:30:00'
location: VALUG-Klubraum im Alten Schl8hof Wels
---
{{% comment %}} page was renamed from Index/Vergangene Veranstaltungen/2015-05-13 Webscraping mit Scrapy{{% /comment %}}
{{% comment %}} page was renamed from Index/Vergangene Veranstaltungen/2015-05-13 scrapy{{% /comment %}}
# Webscraping mit Scrapy
 * **Datum**: Freitag, 13. Mai 2015 18:30
 * **Ort**: VALUG-Klubraum im Alten Schl8hof Wels
 * **Details**: Vortrag von notizblock

## Inhalt
 * Was ist Webscraping
 * Anwendungsgebiete
 * Scrapy
 * Demos
 * Diskussion

## Slides
 * **Git-Repository: https://gitlab.com/valug/notizblock-scrapy-slides** 
 * **Folien:** [Download](http://nblock.org/static/talks/slides-scrapy.pdf)
 * **Demos:** https://gitlab.com/valug/notizblock-scrapy-demos
