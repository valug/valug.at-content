---
date: '2013-07-13 12:53:54'
moinmoin_path: /Index(2f)Vergangene(20)Veranstaltungen(2f)2013(2d)07(2d)12(20)Finanzverwaltung(20)mit(20)freier(20)Software/revisions/00000002
title: 2013-07-12 Finanzverwaltung mit freier Software
start: '2013-07-12 18:30:00'
location: Workshop mit notizblock und s.n.
---
# Finanzverwaltung mit freier Software
 * **Datum**: Freitag, 12.Juli 2013 18:30
 * **Ort**: VALUG-Klubraum im Alten Schl8hof Wels
 * **Details**: Workshop mit notizblock und s.n.

Themen:
 * Warum private Finanzverwaltung?
 * Vorstellung von !GnuCash
 * Dateneingabe in !GnuCash
 * Berichte und Auswertungen mit !GnuCash
 * Showcase der wichtigsten Features für die private Nutzung
 * Diskussion

## Slides
Die Folien sowie Beispieldateien, Rechnung und Anleitungen stehen auf Anfrage gerne zur Verfügung.
