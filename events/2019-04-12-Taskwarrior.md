---
date: '2019-04-13 09:30:24'
moinmoin_path: /Index(2f)Vergangene(20)Veranstaltungen(2f)2019(2d)04(2d)12(20)Taskwarrior/revisions/00000002
title: 2019-04-12 Taskwarrior
start: '2019-04-12 18:30:00'
location: VALUG-Klubraum im Alten Schl8hof Wels
---
# Ein Blick auf SSH
 * **Datum**: Freitag, 12. April 2019 18:30
 * **Ort**: VALUG-Klubraum im Alten Schl8hof Wels
 * **Details**: Vortrag von notizblock
 * **Folien:** [Download](https://nblock.org/static/talks/slides-valug-taskwarrior.pdf)
