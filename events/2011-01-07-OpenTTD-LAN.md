---
date: '2011-01-11 20:58:58'
moinmoin_path: /Index(2f)Vergangene(20)Veranstaltungen(2f)2011(2d)01(2d)07(20)OpenTTD(2d)LAN/revisions/00000004
title: 2011-01-07 OpenTTD-LAN
start: '2011-01-07 18:30:00'
---
# OpenTTD-LAN am 7.1.2011
Anwesend waren 9 Personen. Anfangs wurde ein dedizierter OpenTTD-Server installiert, danach wurde viele Stunden lang OpenTTD gespielt.

## Bilder
{{attachment:DSC_6877_1.JPG}}
{{attachment:DSC_6876_1.JPG}}
{{attachment:DSC_6874_1.JPG}}
{{attachment:DSC_6875_1.JPG}}
{{attachment:DSC_6878_1.JPG}}
{{attachment:DSC_6879_1.JPG}}
{{attachment:DSC_6880_1.JPG}}
