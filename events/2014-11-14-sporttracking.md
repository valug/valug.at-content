---
date: '2014-11-15 08:20:12'
moinmoin_path: /Index(2f)Vergangene(20)Veranstaltungen(2f)2014(2d)11(2d)14(20)sporttracking/revisions/00000002
title: 2014-11-14 sporttracking
start: '2014-11-14 18:30:00'
location: VALUG-Klubraum im Alten Schl8hof Wels
---
# Fitnesstracker unter GNU/Linux
 * **Datum**: Freitag, 14. November 2014 18:30
 * **Ort**: VALUG-Klubraum im Alten Schl8hof Wels
 * **Details**: Vortrag von notizblock

## Inhalt
 * Ein Abriss über die letzten Monate im Bereich Fitnesstracking
 * Gefahren (Zugriff auf Daten, Was lässt sich auswerten, …)
 * Ziele und Möglichkeiten
 * Tools ohne Onlinezwang
 * Demo von MyTourbook
 * Fazit

## Slides
 * **Git-Repository:** https://gitorious.org/valug/notizblock-sporttracker-slides
 * **PDF:** [Download](http://nblock.org/static/talks/slides-sportstracker.pdf)
 * **Notizen:** [Download](http://nblock.org/static/talks/notes-sportstracker.txt)
