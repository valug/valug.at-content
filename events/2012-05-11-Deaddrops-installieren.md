---
date: '2012-05-23 14:15:31'
moinmoin_path: /Index(2f)Vergangene(20)Veranstaltungen(2f)2012(2d)05(2d)11(20)Deaddrops(20)installieren/revisions/00000002
title: 2012-05-11 Deaddrops installieren
start: '2012-05-11 18:30:00'
---
# Deaddrops installieren am 11.5.2012
Eine Gruppe VALUGer/innen haben am 11.5.2012 zwei Deaddrops installiert. Informationen über Deaddrops sind auf der Seite http://deaddrops.com/ zu finden. Die Orte der installierten Drops sind unter http://deaddrops.com/db/?page=view&id=1017 und http://deaddrops.com/db/?page=view&id=1018 zu finden.

[Deaddrops](/pages/Deaddrops)
